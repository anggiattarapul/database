-- 02
alter table "Dosen"
alter column "Nama_Dosen" type varchar(200)
--03
select *
from "Mahasiswa" As MHS
	join "Jurusan" as JUR on
		MHS."Kode_Jurusan"= JUR."Kode_Jurusan"
	join "Agama" as AGH on
		MHS."Kode_Agama" = AGH."Kode_Agama"
where MHS."Kode_Mahasiswa" ='M001'
--4
select *
from "Mahasiswa" As MHS
	join "Jurusan" as JUR on
	MHS."Kode_Jurusan" = JUR."Kode_Jurusan"
where "Status_Jurusan" = 'NonAktif'
--5
select distinct MHS.* 
from "Mahasiswa" as MHS
	Join "Nilai" as Nill On
	Mhs."Kode_Mahasiswa" = Nill."Kode_Mahasiswa"
	Join "Ujian" as UJI on
	Nill."Kode_Ujian" = Uji."Kode_Ujian"
where "Status_Ujian" = 'Aktif' AND "Nilai" > '80'
--6
select distinct MHS.*
from "Mahasiswa" as MHS
	Join "Nilai" as Nill On
	Mhs."Kode_Mahasiswa" = Nill."Kode_Mahasiswa"
where  "Nilai" >= (Select avg ("Nilai")from "Nilai")
--7
select distinct MHS.*
from "Mahasiswa" as MHS
	Join "Nilai" as Nill On
	Mhs."Kode_Mahasiswa" = Nill."Kode_Mahasiswa"
where  "Nilai" <= (Select avg ("Nilai")from "Nilai")

--8
alter table "Jurusan"
add "Biaya" Numeric(10,2)

Kode_Jurusan	Biaya
J001			450000
J002			475000
J003			455000
J004			470000
J005			450000

update "Jurusan" set "Biaya" = 450000 where "Kode_Jurusan" = 'J001'
update "Jurusan" set "Biaya" = 475000 where "Kode_Jurusan" = 'J002'
update "Jurusan" set "Biaya" = 455000 where "Kode_Jurusan" = 'J003'
update "Jurusan" set "Biaya" = 470000 where "Kode_Jurusan" = 'J004'
update "Jurusan" set "Biaya" = 450000 where "Kode_Jurusan" = 'J005'

alter table public."Jurusan"
	alter column "Biaya" Set NOT NULL
select * from "Jurusan"

--9
select sum("Biaya")as JML
from "Jurusan"
where "Status_Jurusan" ='Aktif'

--10
select * 
from "Dosen" as DSN 
	Join "Jurusan" as JUR on
	DSN."Kode_Jurusan" = JUR."Kode_Jurusan"
order by "Biaya" DESC

--11
select * 
from "Jurusan"
where "Nama_Jurusan" like '%Sistem%'

--12
select MHS."Kode_Mahasiswa",MHS."Nama_Mahasiswa", Count(MHS."Kode_Mahasiswa")As JML 
from "Mahasiswa" as MHS
	join "Nilai" as NILL on
	MHS."Kode_Mahasiswa" = NILL."Kode_Mahasiswa"
group by MHS."Kode_Mahasiswa",MHS."Nama_Mahasiswa"
having count(MHS."Kode_Mahasiswa") > 1

--13
select MHS."Kode_Mahasiswa",
		MHS."Nama_Mahasiswa",
		Jur."Nama_Jurusan",
		AGM."Deskripsi" AS "Agama",
		DOS."Nama_Dosen",
		JUR."Status_Jurusan",
		TYD."Deskripsi"
from "Mahasiswa" as MHS
	join "Jurusan" as JUR on
		MHS."Kode_Jurusan" = JUR."Kode_Jurusan"
	join "Agama" as AGM on
		MHS."Kode_Agama" = AGM."Kode_Agama"
	join "Dosen" as DOS on
		JUR."Kode_Jurusan" = DOS."Kode_Jurusan"
	join "Type_Dosen" as TYD on
		DOS."Kode_Type_Dosen" = TYD."Kode_Type_Dosen"
where "Kode_Mahasiswa" = 'M001'
	
	
	
--14
select * from "ViewMahasiswa"
--15
select MHS.*,"nil"."Nilai"
from "Mahasiswa" mhs
	Left join "Nilai" "nil" ON
		MHS."Kode_Mahasiswa" = "nil"."Kode_Mahasiswa"
	



